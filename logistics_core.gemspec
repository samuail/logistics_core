$:.push File.expand_path('../lib', __FILE__)

# Maintain your gem's version:
require 'logistics/core/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'logistics_core'
  s.version     = Logistics::Core::VERSION
  s.authors     = ['Henock L.']
  s.email       = ['henockl@live.com']
  s.homepage    = 'http://www.mks.com.et'
  s.summary     = 'Core classes for MKS logistics apps'
  s.description = 'Core classes for MKS logistics apps'
  s.license     = 'MIT'

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  s.add_dependency 'rails', '~> 5.0.0', '>= 5.0.0.1'

  s.add_development_dependency 'rspec-rails', '~> 3.5', '>= 3.5.2'

  s.add_development_dependency 'factory_girl_rails', '~> 4.7', '>= 4.7.0'

  s.add_development_dependency 'ffaker', '~> 2.2', '>= 2.2.0'

  s.add_development_dependency 'pg', '~> 0.19', '>= 0.19.0'

  s.add_dependency 'bcrypt', '~> 3.1', '>= 3.1.11'

  s.add_dependency 'mks_common', '~> 0.1', '>= 0.1.0'
end
