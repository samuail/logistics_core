class CreateLogisticsCurrencyRates < ActiveRecord::Migration[5.0]
  def change
    create_table :logistics_core_currency_rates do |t|
      t.float :rate_to_base_selling, null: false
      t.float :rate_to_base_buying
      t.date :rate_date, null: false, default: Date.today
      t.references :currency, index: { name: 'currency_on_currency_rate_indx' }, null: false

      t.timestamps
    end

    add_foreign_key :logistics_core_currency_rates, :logistics_core_lookups, :column => :currency_id
  end
end
