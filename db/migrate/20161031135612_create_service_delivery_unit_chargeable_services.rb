class CreateServiceDeliveryUnitChargeableServices < ActiveRecord::Migration[5.0]
  def change
    create_table :logistics_core_service_delivery_unit_chargeable_services do |t|
      t.references :service_delivery_unit, index: false
      t.references :chargeable_service, index: false
    end

    add_index :logistics_core_service_delivery_unit_chargeable_services,
              [:service_delivery_unit_id, :chargeable_service_id],
              name: 'sdu_on_services_indx'

    add_foreign_key :logistics_core_service_delivery_unit_chargeable_services, :logistics_core_service_delivery_units,
        :column => :service_delivery_unit_id
    add_foreign_key :logistics_core_service_delivery_unit_chargeable_services, :logistics_core_chargeable_services,
        :column => :chargeable_service_id
  end
end
