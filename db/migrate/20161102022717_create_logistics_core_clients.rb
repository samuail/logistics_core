class CreateLogisticsCoreClients < ActiveRecord::Migration[5.0]
  def change
    create_table :logistics_core_clients do |t|
      t.string :name, null: false
      t.string :postal_code
      t.string :city
      t.references :country, index: true
      t.string :telephone
      t.string :fax
      t.string :email
      t.string :license
      t.string :tin

      t.timestamps
    end
    add_foreign_key :logistics_core_clients, :logistics_core_lookups, :column => :country_id
  end
end
