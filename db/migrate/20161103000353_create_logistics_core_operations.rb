class CreateLogisticsCoreOperations < ActiveRecord::Migration[5.0]
  def change
    create_table :logistics_core_operations do |t|
      t.string :code, null: false, unique: true
      t.string :contract_number, null: false, unique: true
      t.references :client, index: { name: 'client_on_operation_indx' }
      t.references :customs_office, index: { name: 'customs_office_on_operation_indx' }, null: true
      t.references :customs_office_unit, indx: { name: 'customs_office_unit_on_operation_indx' }, null: true

      t.timestamps
    end

    add_foreign_key :logistics_core_operations, :logistics_core_clients, :column => :client_id
    add_foreign_key :logistics_core_operations, :logistics_core_customs_offices, :column => :customs_office_id
    add_foreign_key :logistics_core_operations, :logistics_core_lookups, :column => :customs_office_unit_id
  end
end
