class CreateLogisticsCoreServiceDeliveryUnits < ActiveRecord::Migration[5.0]
  def change
    create_table :logistics_core_service_delivery_units do |t|
      t.string :code, null: false, unique: true
      t.string :name, null: false, unique: true
      t.references :currency, index: { name: 'currency_on_sdu_indx' }, null: false
      t.string :address
      t.references :service_delivery_unit_type, index: { name: 'sdu_type_on_sdu_indx' }, null: false

      t.timestamps
    end

    add_foreign_key :logistics_core_service_delivery_units, :logistics_core_lookups, :column => :currency_id
    add_foreign_key :logistics_core_service_delivery_units, :logistics_core_lookups, :column => :service_delivery_unit_type_id
  end
end
