class CreateLogisticsCoreExtendedLookups < ActiveRecord::Migration[5.0]
  def change
    create_table :logistics_core_extended_lookups do |t|
      t.string :code, null: false
      t.string :name, null: false
      t.string :description
      t.string :type, null: false

      t.timestamps
    end
    add_index :logistics_core_extended_lookups, [:code, :type], { :unique => true }
  end
end
