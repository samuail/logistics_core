class CreateLogisticsCoreCustomsOffices < ActiveRecord::Migration[5.0]
  def change
    create_table :logistics_core_customs_offices do |t|
      t.string :code, null: false, unique: true
      t.string :name, null: false, unique: true
      t.text :location, null: false

      t.timestamps
    end
  end
end
