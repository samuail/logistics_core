class CreateLogisticsCoreChargeableServices < ActiveRecord::Migration[5.0]
  def change
    create_table :logistics_core_chargeable_services do |t|
      t.string :code, null: false, unique: true
      t.string :name, null: false, unique: true
      t.integer :order

      t.timestamps
    end
  end
end
