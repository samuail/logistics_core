module LookupCommon
  extend ActiveSupport::Concern

  included do
    before_action :set_lookup, only: [:update]
  end

  def index
    clazz = get_model
    response = Mks::Common::MethodResponse.new(true, nil, clazz.all, nil, nil)
    render json: response
  end

  def create
    clazz = get_model
    obj = clazz.new(lookup_params)
    class_name = get_model_name.split('::').last.underscore.humanize
    if obj.save
      response = Mks::Common::MethodResponse.new(true, "#{class_name} saved successfully!",obj, nil, nil)
    else
      errors = Mks::Common::Util.error_messages obj, class_name
      response = Mks::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  def update
    class_name = get_model_name.split('::').last.underscore.humanize
    if @lookup.update(lookup_params)
      response = Mks::Common::MethodResponse.new(true, "#{class_name} updated successfully!", @lookup, nil, nil)
    else
      errors = Mks::Common::Util.error_messages @lookup, class_name
      response = Mks::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  def get_model
    get_model_name.constantize
  end

  def get_model_name
    name = self.class.to_s
    name.slice!('Controller')
    name.singularize
  end

  private

  def set_lookup
    clazz = get_model
    @lookup = clazz.find(params[:id])
  end

  def lookup_params
    params.require(:lookup).permit(:code, :name)
  end
end