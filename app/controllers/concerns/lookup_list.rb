module LookupList
  extend ActiveSupport::Concern

  def lookup
    clazz = get_model
    lookups = clazz.order(:name).select('id, name')
    response = Mks::Common::MethodResponse.new(true, nil, lookups, nil)
    render json: response
  end

  private

  def get_model
    get_model_name.constantize
  end

  def get_model_name
    name = self.class.to_s
    name.slice!('Controller')
    name.singularize
  end
end