module ControllerCommon
  extend ActiveSupport::Concern

  included do
    before_action :set_model, only: [:update]
  end

  def create
    model = get_model.new(model_params)

    if model.save
      response=Mks::Common::MethodResponse.new(true, "#{get_humanized_name} saved successfully!", nil, nil)
    else
      errors = Mks::Common::Util.error_messages(model, "#{get_humanized_name}")
      response = Mks::Common::MethodResponse.new(false, nil, nil, errors, 0)
    end
    render json: response
  end

  def update
    if @model.update(model_params)
      response = Mks::Common::MethodResponse.new(true, "#{get_humanized_name} updated successfully!", @model, nil, nil)
    else
      errors = Mks::Common::Util.error_messages(@model, "#{get_humanized_name}")
      response = Mks::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  private

  def get_model_name
    name = self.class.to_s
    name.slice!('Controller')
    name.singularize
  end

  def get_model
    get_model_name.constantize
  end

  def get_humanized_name
    get_model_name.rpartition('::').last.underscore.humanize
  end

  def model_params
    nil
  end

  def set_model
    @model = get_model.find(params[:id])
  end
end