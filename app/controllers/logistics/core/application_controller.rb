require 'mks/common/methodresponse'
require 'mks/common/util'

module Logistics
  module Core
    class ApplicationController < ActionController::Base
      protect_from_forgery with: :null_session
    end
  end
end