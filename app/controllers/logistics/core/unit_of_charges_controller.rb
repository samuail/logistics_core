require_dependency 'application_controller'

module Logistics
  module Core
    class UnitOfChargesController < ApplicationController
      include LookupCommon
    end
  end
end