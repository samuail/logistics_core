require_dependency 'application_controller'

module Logistics
  module Core
    class CurrencyRatesController < ApplicationController
      include ControllerCommon

      def rates
        currency = Currency.find(params[:id])
        rates = currency.rates.order('rate_date DESC')
        response = Mks::Common::MethodResponse.new(true, nil, rates, nil, nil)
        render json: response
      end


      private

      def model_params
        params.require(:rate).permit(:rate_to_base_buying, :rate_to_base_selling, :rate_date, :currency_id)
      end
    end
  end
end