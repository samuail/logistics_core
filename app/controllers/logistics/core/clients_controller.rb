require_dependency 'application_controller'

module Logistics
  module Core
    class ClientsController < ApplicationController

      include ControllerCommon
      include LookupList

      def index
        clients = Client.order(:name)
        clients_array = []
        clients.each{ |c|
          clients_array.push({'id'=>c.id, 'name' => c.name, 'postal_code'=>c.postal_code, 'city'=>c.city,
                              'country_id'=>c.country_id, 'country_name'=>c.country.name, 'fax'=> c.fax,
                              'telephone' => c.telephone, 'email'=>c.email, 'license'=>c.license, 'tin'=>c.tin})
        }
        response = Mks::Common::MethodResponse.new(true, nil, clients_array, nil)
        render json: response
      end


      private

      def model_params
        params.require(:client).permit(:name, :postal_code, :city, :country_id, :telephone, :fax, :email, :license, :tin)
      end
      
    end
  end
end