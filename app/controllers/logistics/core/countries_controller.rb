require_dependency 'application_controller'

module Logistics
  module Core
    class CountriesController < ApplicationController
      include LookupCommon
    end
  end
end