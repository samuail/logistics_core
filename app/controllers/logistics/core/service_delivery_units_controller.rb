require_dependency 'application_controller'

module Logistics
  module Core
    class ServiceDeliveryUnitsController < ApplicationController
      include ControllerCommon

      def index
        @sdus = ServiceDeliveryUnit.all
        service_delivery_units_array = []
        @sdus.each { |sdu|
          service_delivery_units_array.push({:id => sdu.id, :code => sdu.code, :name => sdu.name, :address => sdu.address,
                                             :currency_id => sdu.currency_id, :currency_name => sdu.currency.name,
                                             :service_delivery_unit_type_id => sdu.service_delivery_unit_type_id,
                                             :type_name => sdu.service_delivery_unit_type.name })
        }
        @response = {:success => true, :message => '', :data => service_delivery_units_array}
        render json: @response
      end


      def unassigned_services
        sdu = ServiceDeliveryUnit.find(params[:id])
        ids = sdu.chargeable_services.map(&:id)
        unassigned_services = ChargeableService.where.not(id: ids)
        response = Mks::Common::MethodResponse.new(true, nil, unassigned_services, nil, nil)
        render json: response
      end
      

      private

      # Never trust parameters from the scary internet, only allow the white list through.
      def model_params
        params.require(:service_delivery_unit).permit(:code, :name, :address, :currency_id, :service_delivery_unit_type_id)
      end
    end
  end
end