require_dependency 'application_controller'

module Logistics
  module Core
    class CustomsOfficeUnitsController < ApplicationController
      include LookupCommon
    end
  end
end