require_dependency 'application_controller'

module Logistics
  module Core
    class ServiceDeliveryUnitTypesController < ApplicationController
      include LookupCommon
    end
  end
end