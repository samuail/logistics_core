module Logistics
	module Core
	  class Currency < Lookup
      has_many :rates, class_name: 'Logistics::Core::CurrencyRate'
	  end
	end
end