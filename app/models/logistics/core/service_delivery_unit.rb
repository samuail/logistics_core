module Logistics
  module Core
    class ServiceDeliveryUnit < ApplicationRecord
      validates :code, :name, presence: true, uniqueness: true

      belongs_to :currency
      belongs_to :service_delivery_unit_type
      has_and_belongs_to_many :chargeable_services, class_name: 'Logistics::Core::ChargeableService',
          join_table: 'logistics_core_service_delivery_unit_chargeable_services'
    end
  end
end