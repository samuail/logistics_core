module Logistics
  module Core
    class CustomsOffice < ApplicationRecord
      validates :code, :name, presence: true, uniqueness: true
    end
  end
end