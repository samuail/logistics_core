module Logistics
  module Core
    class Client < ApplicationRecord
      VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

      validates :name, presence: true
      validates :email, format: { with: VALID_EMAIL_REGEX }
      belongs_to :country

      before_save { email.downcase! }
    end
  end
end