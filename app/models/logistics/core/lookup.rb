module Logistics
  module Core
    class Lookup < ApplicationRecord
      validates :code, presence: true, uniqueness: { scope: [:code, :type] }
      validates :name, :type, presence: true, uniqueness: { scope: [:name, :type] }
    end
  end
end