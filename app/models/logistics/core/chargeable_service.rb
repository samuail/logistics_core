module Logistics
  module Core
    class ChargeableService < ApplicationRecord
      validates :code, :name, presence: true, uniqueness: true
    end
  end
end