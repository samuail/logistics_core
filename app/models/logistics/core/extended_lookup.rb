module Logistics
  module Core
    class ExtendedLookup < ApplicationRecord
      validates :code, presence: true, uniqueness: {scope: [:code, :type]}
      validates :name, :type, presence: true
    end
  end
end