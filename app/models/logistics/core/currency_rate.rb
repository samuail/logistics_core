module Logistics
	module Core
	  class CurrencyRate < ApplicationRecord
			validates :rate_to_base_selling, :rate_date, presence: true
	    belongs_to :currency
	  end
	end
end