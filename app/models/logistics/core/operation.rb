module Logistics
  module Core
    class Operation < ApplicationRecord
      validates :code, :contract_number, presence: true, uniqueness: true
      validates :client, presence: true

      belongs_to :client
      belongs_to :customs_office, optional: true
      belongs_to :customs_office_unit, optional: true
    end
  end
end