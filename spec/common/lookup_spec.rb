module LookupSpec
  extend ActiveSupport::Concern

  def get_model_name
    name = self.class.to_s.split('::').select{|s| s.include? 'LogisticsCore'}.first
    name.slice!('Controller')
    name.singularize.split('::').last.sub('LogisticsCore', '')
  end

  def get_humanized
    get_model_name.underscore.humanize
  end

  def get_model_sym
    get_model_name.underscore.to_sym
  end

  def get_model
    "Logistics::Core::#{get_model_name}".constantize
  end

  def get_index
    2.times { create(get_model_sym) }
    get :index, format: :json
    result = JSON(response.body)
    expect(result['data'].count).to eq 2
  end

  def post_valid
    expect {
      post :create, params: {lookup: valid_attributes}, format: :json
    }.to change(get_model, :count).by(1)
  end

  def post_message
    post :create, params: {lookup: valid_attributes}, format: :json
    result = JSON(response.body)
    expect(result['message']).to eq("#{get_humanized} saved successfully!")
  end

  def post_invalid
    post :create, params: {lookup: invalid_attributes}, format: :json
    result = JSON(response.body)
    expect(result['errors']).to include("#{get_humanized} Code can't be blank")
  end

  def put_valid
    lookup = create(get_model_sym)
    old_data = lookup.name
    put :update, params: {id: lookup.to_param, lookup: new_attributes}, format: :json
    lookup.reload
    expect(lookup.name).not_to eq old_data
  end

  def put_message
    lookup = create(get_model_sym)
    put :update, params: {id: lookup.to_param, lookup: valid_attributes}, format: :json
    result = JSON(response.body)
    expect(result['message']).to eq("#{get_humanized} updated successfully!")
  end

  def put_invalid
    lookup = create(get_model_sym)
    put :update, params: {id: lookup.to_param, lookup: invalid_attributes}, format: :json
    result = JSON(response.body)
    expect(result['errors']).to include("#{get_humanized} Code can't be blank")
  end

end
