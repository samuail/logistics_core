require 'rails_helper'

module Logistics
  module Core
    RSpec.describe ExtendedLookup, type: :model do
      it 'has a valid factory' do
        expect(create(:extended_lookup)).to be_valid
      end

      it 'is invalid with no code' do
        expect(build(:extended_lookup, :code => nil)).not_to be_valid
      end

      it 'is invalid with no name' do
        expect(build(:extended_lookup, :name => nil)).not_to be_valid
      end

      it 'is invalid with no type' do
        expect(build(:extended_lookup, type: nil)).not_to be_valid
      end

      it 'is invalid with duplicate code and type' do
        lookup = create(:extended_lookup)
        expect(build(:extended_lookup, :code => lookup.code)).not_to be_valid
      end
    end
  end
end