require 'rails_helper'

module Logistics
  module Core
    RSpec.describe CustomsOfficeUnit, type: :model do
      it 'has a valid factory' do
        expect(create(:customs_office_unit)).to be_valid
      end
    end
  end
end