require 'rails_helper'

module Logistics
  module Core
    RSpec.describe Client, type: :model do
      it 'has a valid factory' do
        expect(create(:client)).to be_valid
      end

      it 'is invalid with no name' do
        expect(build(:client, name: nil)).not_to be_valid
      end

      it 'is invalid with invalid email' do
        expect(build(:client, email: '123456.com')).not_to be_valid
      end
    end
  end
end