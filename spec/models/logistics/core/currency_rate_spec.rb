require 'rails_helper'

module Logistics
	module Core
	  RSpec.describe CurrencyRate, type: :model do
	    it 'has a valid factory' do
	    	expect(create(:currency_rate)).to be_valid
			end

			it 'is invalid with no selling rate' do
        expect(build(:currency_rate, rate_to_base_selling: nil)).not_to be_valid
      end

      it 'is invalid with no rate date' do
        expect(build(:currency_rate, rate_date: nil)).not_to be_valid
      end

      it 'is invalid with no currency' do
        expect(build(:currency_rate, currency: nil)).not_to be_valid
      end
	  end
	end
end
