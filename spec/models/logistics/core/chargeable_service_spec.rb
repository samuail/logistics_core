require 'rails_helper'

module Logistics
  module Core
    RSpec.describe Core::ChargeableService, type: :model do
      it 'has a valid factory' do
        expect(create(:chargeable_service)).to be_valid
      end

      it 'is invalid with no code' do
        expect(build(:chargeable_service, code: nil)).not_to be_valid
      end

      it 'is invalid with no name' do
        expect(build(:chargeable_service, name: nil)).not_to be_valid
      end

      it 'is invalid with duplicate code' do
        sdu = create(:chargeable_service)
        expect(build(:chargeable_service, code: sdu.code)).not_to be_valid
      end

      it 'is invalid with duplicate name' do
        sdu = create(:chargeable_service)
        expect(build(:chargeable_service, name: sdu.name)).not_to be_valid
      end

    end
  end
end