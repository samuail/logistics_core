require 'rails_helper'

module Logistics
  module Core
    RSpec.describe OperationType, type: :model do
      it 'has a valid factory' do
        expect(create(:operation_type)).to be_valid
      end
    end
  end
end