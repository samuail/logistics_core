require 'rails_helper'

module Logistics
  module Core
    RSpec.describe Lookup, type: :model do
      it 'has a valid factory' do
        expect(create(:lookup)).to be_valid
      end

      it 'is invalid with no code' do
        expect(build(:lookup, :code => nil)).not_to be_valid
      end

      it 'is invalid with no name' do
        expect(build(:lookup, :name => nil)).not_to be_valid
      end

      it 'is invalid with no type' do
        expect(build(:lookup, type: nil)).not_to be_valid
      end

      it 'is invalid with duplicate code and type' do
        lookup = create(:lookup)
        expect(build(:lookup, :code => lookup.code)).not_to be_valid
      end
    end
  end
end