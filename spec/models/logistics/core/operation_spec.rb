require 'rails_helper'

module Logistics
  module Core
    RSpec.describe Operation, type: :model do
      it 'has a valid factory' do
        expect(create(:operation)).to be_valid
      end

      it 'is invalid with no code' do
        expect(build(:operation, :code => '')).not_to be_valid
      end

      it 'is invalid with no client' do
        expect(build(:operation, :client => nil)).not_to be_valid
      end

      it 'is invalid with no contract number' do
        expect(build(:operation, :contract_number => '')).not_to be_valid
      end

      it 'is invalid with duplicate code' do
        op = create(:operation)
        expect(build(:operation, :code => op.code)).not_to be_valid
      end

      it 'is invalid with duplicate contract number' do
        op = create(:operation)
        expect(build(:operation, :contract_number => op.contract_number)).not_to be_valid
      end
    end
  end
end