require 'rails_helper'

module Logistics
  module Core
    RSpec.describe ServiceDeliveryUnit, type: :model do
      it 'has a valid factory' do
        expect(create(:service_delivery_unit)).to be_valid
      end

      it 'is invalid with no code' do
        expect(build(:service_delivery_unit, code: nil)).not_to be_valid
      end

      it 'is invalid with no name' do
        expect(build(:service_delivery_unit, name: nil)).not_to be_valid
      end

      it 'is invalid with duplicate code' do
        sdu = create(:service_delivery_unit)
        expect(build(:service_delivery_unit, code: sdu.code)).not_to be_valid
      end

      it 'is invalid with duplicate name' do
        sdu = create(:service_delivery_unit)
        expect(build(:service_delivery_unit, name: sdu.name)).not_to be_valid
      end

      it 'is invalid with no currency' do
        expect(build(:service_delivery_unit, currency: nil)).not_to be_valid
      end

      it 'is invalid with no service delivery unit type' do
        expect(build(:service_delivery_unit, service_delivery_unit_type: nil)).not_to be_valid
      end

      it 'can access chargeable services' do
        sdu = create(:service_delivery_unit)
        sdu.chargeable_services = [create(:chargeable_service), create(:chargeable_service)]
        sdu.reload
        expect(sdu.chargeable_services.count).to eq 2
      end

    end
  end
end