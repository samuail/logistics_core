require 'rails_helper'

module Logistics
  module Core
    RSpec.describe TransactionType, type: :model do
      it 'has a valid factory' do
        expect(create(:transaction_type)).to be_valid
      end
    end
  end
end