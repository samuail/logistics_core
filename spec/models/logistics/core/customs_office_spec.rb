require 'rails_helper'

module Logistics
  module Core
    RSpec.describe CustomsOffice, type: :model do
      it 'has a valid factory' do
        expect(create(:customs_office)).to be_valid
      end

      it 'is invalid with no code' do
        expect(build(:customs_office, :code => '')).not_to be_valid
      end

      it 'is invalid with no name' do
        expect(build(:customs_office, :name => nil)).not_to be_valid
      end

      it 'is invalid with duplicate code' do
        customs_office = create(:customs_office)
        expect(build(:customs_office, :code => customs_office.code)).not_to be_valid
      end

      it 'is invalid with duplicate name' do
        customs_office = create(:customs_office)
        expect(build(:customs_office, :name => customs_office.name)).not_to be_valid
      end
    end
  end
end