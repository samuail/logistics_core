require 'rails_helper'

module Logistics
  module Core
    RSpec.describe ServiceDeliveryUnitType, type: :model do
      it 'has a valid factory' do
        expect(create(:service_delivery_unit_type)).to be_valid
      end
    end
  end
end