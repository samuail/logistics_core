require 'rails_helper'

module Logistics
	module Core
	  RSpec.describe Currency, type: :model do
	    it 'has a valid factory' do
	    	expect(create(:currency)).to be_valid
			end

      it 'can access rates' do
        c = create(:currency, :with_rates)
        expect(c.rates.count).to eq 2
      end
	  end
	end
end