FactoryGirl.define do
  factory :currency_rate, class: 'Logistics::Core::CurrencyRate' do
    rate_to_base_selling 20
    rate_to_base_buying 30
    rate_date { Date.today }
    association :currency
  end
end
