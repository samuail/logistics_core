FactoryGirl.define do
  factory :customs_office, class: 'Logistics::Core::CustomsOffice' do
    code { FFaker::Name.name }
    name { FFaker::Name.name }
    location { FFaker::Name.name }
  end
end
