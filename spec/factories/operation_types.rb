FactoryGirl.define do
  factory :operation_type, parent: :lookup, class: 'Logistics::Core::OperationType' do
    type 'Logistics::Core::OperationType'
  end
end
