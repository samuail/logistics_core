FactoryGirl.define do
  factory :service_delivery_unit, class: 'Logistics::Core::ServiceDeliveryUnit' do
    code { FFaker::Name.name }
    name { FFaker::Name.name }
    association :currency
    address { FFaker::Name.name }
    association :service_delivery_unit_type
  end
end
