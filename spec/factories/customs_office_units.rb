FactoryGirl.define do
  factory :customs_office_unit, parent: :lookup, class: 'Logistics::Core::CustomsOfficeUnit' do
    type 'Logistics::Core::CustomsOfficeUnit'
  end
end
