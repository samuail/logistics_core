FactoryGirl.define do
  factory :extended_lookup, class: 'Logistics::Core::ExtendedLookup' do
    code { FFaker::Name.name }
    name { FFaker::Name.name }
    description { FFaker::Name.name }
    type 'ExtendedLookup'
  end
end
