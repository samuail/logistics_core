FactoryGirl.define do
  factory :lookup, class: 'Logistics::Core::Lookup' do
    code { FFaker::Name.name }
    name { FFaker::Name.name }
    type 'Lookup'
  end
end
