FactoryGirl.define do
  factory :service_delivery_unit_type, parent: :lookup, class: 'Logistics::Core::ServiceDeliveryUnitType' do
    type 'Logistics::Core::ServiceDeliveryUnitType'
  end
end
