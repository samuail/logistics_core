FactoryGirl.define do
  factory :country, parent: :lookup, class: 'Logistics::Core::Country' do
    type 'Logistics::Core::Country'
  end
end
