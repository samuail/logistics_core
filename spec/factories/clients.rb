FactoryGirl.define do
  factory :client, class: 'Logistics::Core::Client' do
    name { FFaker::Name.name }
    postal_code { FFaker::Name.name }
    city { FFaker::Name.name }
    association :country
    telephone { FFaker::Name.name }
    email { FFaker::Internet.email }
    license { FFaker::Name.name }
    tin { FFaker::Name.name }
  end
end
