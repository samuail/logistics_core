FactoryGirl.define do
  factory :transaction_type, parent: :lookup, class: 'Logistics::Core::TransactionType' do
    type 'Logistics::Core::TransactionType'
  end
end
