FactoryGirl.define do
  factory :operation, class: 'Logistics::Core::Operation' do
    code { FFaker::Name.name }
    contract_number { FFaker::Name.name }
    association :client
    association :customs_office
    association :customs_office_unit
  end
end
