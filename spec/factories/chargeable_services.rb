FactoryGirl.define do
  factory :chargeable_service, class: 'Logistics::Core::ChargeableService' do
    code { FFaker::Name.name }
    name { FFaker::Name.name }
    sequence(:order, 1) { |n| n }
  end
end
