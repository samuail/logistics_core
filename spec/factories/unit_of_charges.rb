FactoryGirl.define do
  factory :unit_of_charge, parent: :lookup, class: 'Logistics::Core::UnitOfCharge' do
    type 'Logistics::Core::UnitOfCharge'
  end
end
