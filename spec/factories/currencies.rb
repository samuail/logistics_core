FactoryGirl.define do
  factory :currency, parent: :lookup, class: 'Logistics::Core::Currency' do
    type 'Logistics::Core::Currency'
    trait :with_rates do
      after(:create) do |cur|
        cur.rates = [create(:currency_rate, currency: cur), create(:currency_rate, currency: cur)]
      end
    end
  end
end
