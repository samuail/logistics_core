# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161103000353) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "logistics_core_chargeable_services", force: :cascade do |t|
    t.string   "code",       null: false
    t.string   "name",       null: false
    t.integer  "order"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "logistics_core_clients", force: :cascade do |t|
    t.string   "name",        null: false
    t.string   "postal_code"
    t.string   "city"
    t.integer  "country_id"
    t.string   "telephone"
    t.string   "fax"
    t.string   "email"
    t.string   "license"
    t.string   "tin"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["country_id"], name: "index_logistics_core_clients_on_country_id", using: :btree
  end

  create_table "logistics_core_currency_rates", force: :cascade do |t|
    t.float    "rate_to_base_selling",                        null: false
    t.float    "rate_to_base_buying"
    t.date     "rate_date",            default: '2016-11-06', null: false
    t.integer  "currency_id",                                 null: false
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.index ["currency_id"], name: "currency_on_currency_rate_indx", using: :btree
  end

  create_table "logistics_core_customs_offices", force: :cascade do |t|
    t.string   "code",       null: false
    t.string   "name",       null: false
    t.text     "location",   null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "logistics_core_extended_lookups", force: :cascade do |t|
    t.string   "code",        null: false
    t.string   "name",        null: false
    t.string   "description"
    t.string   "type",        null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["code", "type"], name: "index_logistics_core_extended_lookups_on_code_and_type", unique: true, using: :btree
  end

  create_table "logistics_core_lookups", force: :cascade do |t|
    t.string   "code",       null: false
    t.string   "name",       null: false
    t.string   "type",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["code", "type"], name: "index_logistics_core_lookups_on_code_and_type", unique: true, using: :btree
  end

  create_table "logistics_core_operations", force: :cascade do |t|
    t.string   "code",                   null: false
    t.string   "contract_number",        null: false
    t.integer  "client_id"
    t.integer  "customs_office_id"
    t.integer  "customs_office_unit_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["client_id"], name: "client_on_operation_indx", using: :btree
    t.index ["customs_office_id"], name: "customs_office_on_operation_indx", using: :btree
    t.index ["customs_office_unit_id"], name: "index_logistics_core_operations_on_customs_office_unit_id", using: :btree
  end

  create_table "logistics_core_service_delivery_unit_chargeable_services", force: :cascade do |t|
    t.integer "service_delivery_unit_id"
    t.integer "chargeable_service_id"
    t.index ["service_delivery_unit_id", "chargeable_service_id"], name: "sdu_on_services_indx", using: :btree
  end

  create_table "logistics_core_service_delivery_units", force: :cascade do |t|
    t.string   "code",                          null: false
    t.string   "name",                          null: false
    t.integer  "currency_id",                   null: false
    t.string   "address"
    t.integer  "service_delivery_unit_type_id", null: false
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.index ["currency_id"], name: "currency_on_sdu_indx", using: :btree
    t.index ["service_delivery_unit_type_id"], name: "sdu_type_on_sdu_indx", using: :btree
  end

  add_foreign_key "logistics_core_clients", "logistics_core_lookups", column: "country_id"
  add_foreign_key "logistics_core_currency_rates", "logistics_core_lookups", column: "currency_id"
  add_foreign_key "logistics_core_operations", "logistics_core_clients", column: "client_id"
  add_foreign_key "logistics_core_operations", "logistics_core_customs_offices", column: "customs_office_id"
  add_foreign_key "logistics_core_operations", "logistics_core_lookups", column: "customs_office_unit_id"
  add_foreign_key "logistics_core_service_delivery_unit_chargeable_services", "logistics_core_chargeable_services", column: "chargeable_service_id"
  add_foreign_key "logistics_core_service_delivery_unit_chargeable_services", "logistics_core_service_delivery_units", column: "service_delivery_unit_id"
  add_foreign_key "logistics_core_service_delivery_units", "logistics_core_lookups", column: "currency_id"
  add_foreign_key "logistics_core_service_delivery_units", "logistics_core_lookups", column: "service_delivery_unit_type_id"
end
