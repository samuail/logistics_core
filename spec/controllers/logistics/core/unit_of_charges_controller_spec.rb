require 'rails_helper'

module Logistics
  module Core
    RSpec.describe UnitOfChargesController, type: :controller do
      include LookupSpec

      routes { Logistics::Core::Engine.routes }
      let(:valid_attributes) {
        {
          code: FFaker::Name.name,
          name: FFaker::Name.name
        }
      }

      let(:invalid_attributes) {
        {
          code: '',
          name: FFaker::Name.name
        }
      }

      describe 'GET #index' do
        it 'gets all lookup' do
          get_index
        end
      end

      describe 'POST #create' do
        context 'with valid params' do
          it 'creates a new lookup' do
            post_valid
          end

          it 'should return a success message' do
            post_message
          end
        end

        context 'with invalid params' do
          it 'should return an error message' do
            post_invalid
          end
        end
      end

      describe 'PUT #update' do
        context 'with valid params' do
          let(:new_attributes) {
            {
              name: FFaker::Name.name
            }
          }

          it 'updates the requested lookup' do
            put_valid
          end

          it 'should return a success message' do
            put_message
          end
        end

        context 'with invalid params' do
          it 'should return an error message' do
            put_invalid
          end
        end
      end
    end
  end
end