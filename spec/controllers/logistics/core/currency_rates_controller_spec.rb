require 'rails_helper'

module Logistics
  module Core
    RSpec.describe CurrencyRatesController, type: :controller do
      routes { Logistics::Core::Engine.routes }

      let(:valid_attributes) {
        {
          rate_to_base_selling: 20,
          rate_to_base_buying: 30,
          rate_date: Date.today,
          currency_id: create(:currency).id
        }
      }

      let(:invalid_attributes) {
        {
          rate_to_base_selling: nil,
          rate_to_base_buying: 30,
          rate_date: Date.today,
          currency_id: create(:currency).id
        }
      }

      describe 'GET #rates' do
        it 'gets currency rates for a given currency' do
          currency = create(:currency)
          2.times { create(:currency_rate, currency: currency) }
          get :rates, params: { id: currency.id }, format: :json
          result = JSON(response.body)
          expect(result['data'].count).to eq 2
        end
      end

      describe 'POST #create' do
        context 'with valid params' do
          it 'creates a new currency rate' do
            expect {
              post :create, params: {rate: valid_attributes}, format: :json
            }.to change(CurrencyRate, :count).by(1)
          end

          it 'should return a success message' do
            post :create, params: {rate: valid_attributes}, format: :json
            result = JSON(response.body)
            expect(result['message']).to eq('Currency rate saved successfully!')
          end
        end

        context 'with invalid params' do
          it 'should return an error message' do
            post :create, params: {rate: invalid_attributes}, format: :json
            result = JSON(response.body)
            expect(result['errors']).to include("Currency rate Rate to base selling can't be blank")
          end
        end
      end

      describe 'PUT #update' do
        context 'with valid params' do
          let(:new_attributes) {
            {
              rate_to_base_selling: 50
            }
          }

          it 'updates the requested acquisition mode' do
            rate = create(:currency_rate)
            old_data = rate.rate_to_base_selling
            put :update, params: {id: rate.to_param, rate: new_attributes}, format: :json
            rate.reload
            expect(rate.rate_to_base_selling).not_to eq old_data
          end

          it 'should return a success message' do
            rate = create(:currency_rate)
            put :update, params: {id: rate.to_param, rate: valid_attributes}, format: :json
            result = JSON(response.body)
            expect(result['message']).to eq('Currency rate updated successfully!')
          end
        end

        context 'with invalid params' do
          it 'should return an error message' do
            rate = create(:currency_rate)
            put :update, params: {id: rate.to_param, rate: invalid_attributes}, format: :json
            result = JSON(response.body)
            expect(result['errors']).to include("Currency rate Rate to base selling can't be blank")
          end
        end
      end
    end
  end
end