require 'rails_helper'

module Logistics
  module Core
    RSpec.describe ClientsController, type: :controller do
      routes { Logistics::Core::Engine.routes }

      let(:valid_attributes) {
        {
          name: FFaker::Name.name,
          postal_code: FFaker::Name.name,
          city: FFaker::Name.name,
          country_id: create(:country).id,
          telephone: FFaker::Name.name,
          email: FFaker::Internet.email,
          license: FFaker::Name.name,
          tin: FFaker::Name.name
        }
      }

      let(:invalid_attributes) {
        {
          name: nil,
          postal_code: FFaker::Name.name,
          city: FFaker::Name.name,
          country_id: create(:country).id,
          telephone: FFaker::Name.name,
          email: FFaker::Internet.email,
          license: FFaker::Name.name,
          tin: FFaker::Name.name
        }
      }

      describe 'GET #index' do
        it 'gets all clients' do
          2.times { create(:client) }
          get :index, format: :json
          result = JSON(response.body)
          expect(result['data'].count).to eq 2
        end
      end

      describe 'GET #lookup' do
        it 'gets all clients as lookup' do
          2.times { create(:client) }
          get :lookup, format: :json
          result = JSON(response.body)
          expect(result['data'].count).to eq 2
        end
      end
      
      describe 'POST #create' do
        context 'with valid params' do
          it 'creates a new client' do
            expect {
              post :create, params: {client: valid_attributes}, format: :json
            }.to change(Client, :count).by(1)
          end

          it 'should return a success message' do
            post :create, params: {client: valid_attributes}, format: :json
            result = JSON(response.body)
            expect(result['message']).to eq('Client saved successfully!')
          end
        end

        context 'with invalid params' do
          it 'should return an error message' do
            post :create, params: {client: invalid_attributes}, format: :json
            result = JSON(response.body)
            expect(result['errors']).to include("Client Name can't be blank")
          end
        end
      end

      describe 'PUT #update' do
        context 'with valid params' do
          let(:new_attributes) {
            {
              name: FFaker::Name.name
            }
          }

          it 'updates the requested acquisition mode' do
            client = create(:client)
            old_data = client.name
            put :update, params: {id: client.to_param, client: new_attributes}, format: :json
            client.reload
            expect(client.name).not_to eq old_data
          end

          it 'should return a success message' do
            client = create(:client)
            put :update, params: {id: client.to_param, client: valid_attributes}, format: :json
            result = JSON(response.body)
            expect(result['message']).to eq('Client updated successfully!')
          end
        end

        context 'with invalid params' do
          it 'should return an error message' do
            client = create(:client)
            put :update, params: {id: client.to_param, client: invalid_attributes}, format: :json
            result = JSON(response.body)
            expect(result['errors']).to include("Client Name can't be blank")
          end
        end
      end
    end
  end
end