require 'rails_helper'

module Logistics
  module Core
    RSpec.describe ServiceDeliveryUnitsController, type: :controller do
      routes { Logistics::Core::Engine.routes }

      let(:valid_attributes) {
        {
          code: FFaker::Name.name,
          name: FFaker::Name.name,
          currency_id: create(:currency).id,
          address: FFaker::Name.name,
          service_delivery_unit_type_id: create(:service_delivery_unit_type).id
        }
      }

      let(:invalid_attributes) {
        {
          code: nil,
          name: FFaker::Name.name,
          currency_id: create(:currency).id,
          address: FFaker::Name.name,
          service_delivery_unit_type_id: create(:service_delivery_unit_type).id
        }
      }

      describe 'GET #index' do
        it 'gets all service delivery units' do
          2.times { create(:service_delivery_unit) }
          get :index, format: :json
          result = JSON(response.body)
          expect(result['data'].count).to eq 2
        end
      end

      describe 'POST #create' do
        context 'with valid params' do
          it 'creates a new service delivery unit' do
            expect {
              post :create, params: {service_delivery_unit: valid_attributes}, format: :json
            }.to change(ServiceDeliveryUnit, :count).by(1)
          end

          it 'should return a success message' do
            post :create, params: {service_delivery_unit: valid_attributes}, format: :json
            result = JSON(response.body)
            expect(result['message']).to eq('Service delivery unit saved successfully!')
          end
        end

        context 'with invalid params' do
          it 'should return an error message' do
            post :create, params: {service_delivery_unit: invalid_attributes}, format: :json
            result = JSON(response.body)
            expect(result['errors']).to include("Service delivery unit Code can't be blank")
          end
        end
      end

      describe 'PUT #update' do
        context 'with valid params' do
          let(:new_attributes) {
            {
              code: FFaker::Name.name
            }
          }

          it 'updates the requested service delivery unit' do
            sdu = create(:service_delivery_unit)
            old_data = sdu.code
            put :update, params: {id: sdu.to_param, service_delivery_unit: new_attributes}, format: :json
            sdu.reload
            expect(sdu.code).not_to eq old_data
          end

          it 'should return a success message' do
            sdu = create(:service_delivery_unit)
            put :update, params: {id: sdu.to_param, service_delivery_unit: valid_attributes}, format: :json
            result = JSON(response.body)
            expect(result['message']).to eq('Service delivery unit updated successfully!')
          end
        end

        context 'with invalid params' do
          it 'should return an error message' do
            sdu = create(:service_delivery_unit)
            put :update, params: {id: sdu.to_param, service_delivery_unit: invalid_attributes}, format: :json
            result = JSON(response.body)
            expect(result['errors']).to include("Service delivery unit Code can't be blank")
          end
        end
      end
    end
  end
end