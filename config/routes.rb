Logistics::Core::Engine.routes.draw do
  resources :currencies, except: [:new, :edit, :show, :destroy] do
    member do
      get 'rates', :controller => :currency_rates, :action => :rates
    end
  end
  resources :currency_rates, only: [:create, :update]
  resources :countries, except: [:new, :edit, :show, :destroy]
  resources :clients, except: [:new, :edit, :show, :destroy]
  get 'clients/lookup', :to => 'clients#lookup'
  resources :customs_office_units, except: [:new, :edit, :show, :destroy]
  resources :operation_types, except: [:new, :edit, :show, :destroy]
  resources :service_delivery_unit_types, except: [:new, :edit, :show, :destroy]
  resources :transaction_types, except: [:new, :edit, :show, :destroy]
  resources :unit_of_charges, except: [:new, :edit, :show, :destroy]
  resources :service_delivery_units, except: [:new, :edit, :show, :destroy]
end
